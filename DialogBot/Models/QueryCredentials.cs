﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DialogBot.Models
{
    public class QueryCredentials
    {
        public DateTime StartDate { get; set; }
        public string Role { get; set; }
        public string City { get; set; }

    }
}