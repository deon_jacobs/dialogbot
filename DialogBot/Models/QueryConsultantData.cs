﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Azure.Devices.Client;
using System.IO;

namespace DialogBot.Models
{
    public class QueryConsultantData
    {
        CloudBlockBlob blobStorage;
        protected int fileRowCount;
        List<ConsultantDetail> consultantData;

        public QueryConsultantData()
        {
            blobStorage = BlobStorage.BlobConfig("consultantdata", "consultant_list_v2.csv");
            fileRowCount = 4;
            consultantData = new List<ConsultantDetail>();
            GetInitialConsultantData();
        }
        public void GetInitialConsultantData()
        {
            string text = string.Empty;
            DateTime availDate = DateTime.MinValue;
            using (var memoryStream = new MemoryStream())
            {
                //read blob csv file into memoryStream
                blobStorage.DownloadToStream(memoryStream);
                //transfer memorystream to string variable
                text = System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                string[] lines;      //string array to store each record of the blob file
                //convert to string array to access individual rows
                lines = text.Split('\n');
                //Loop through each item witin the string array
                foreach (string row in lines)
                {
                    string date = string.Empty;
                    var consultant = new ConsultantDetail();
                    string[] consultantRow = row.Split(',', '\r');
                    if (consultantRow.Length == fileRowCount + 1)//check that rows have been decoded properly
                    {
                        //assign csv rows to TruckMeasure object
                        consultant.City = consultantRow[0];
                        consultant.Name = consultantRow[1];
                        DateTime.TryParse(consultantRow[2], out availDate);
                        date = consultantRow[2];
                        consultant.AvailableDate = availDate;
                        consultant.Role = consultantRow[3];
                        consultantData.Add(consultant);
                    }
                }
            }
            ConsultStaticStore.AllConsultantData = consultantData;
        }
       

    }
}