﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DialogBot.Models
{
    public static class ConsultStaticStore
    {
        public static List<ConsultantDetail> AllConsultantData; 

        public static List<ConsultantDetail> GetRequestedConsultantData(QueryCredentials queryCred)
        {
            var filter = AllConsultantData.Where(c => c.AvailableDate < queryCred.StartDate && c.City.ToLower() == queryCred.City && c.Role.ToLower() == queryCred.Role).ToList();
            return filter;
        }
    }
}