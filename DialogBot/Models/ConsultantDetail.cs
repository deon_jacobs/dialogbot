﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DialogBot.Models
{
    public class ConsultantDetail
    {
        public DateTime AvailableDate { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Role { get; set; }
    }
}