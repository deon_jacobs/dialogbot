﻿using Microsoft.Bot.Connector;
using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace DialogBot.Models
{
    [Serializable]
    public class GDialogue : IDialog<object>
    {
        protected int count = 1;
        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync); //Waits and calls MessageReceivedAsync when there is a message
        }
        public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> argument)
        {
            var message = await argument;
            if (message.Text == "reset")
            {
                PromptDialog.Confirm(
                    context,                                            //IDialog context
                    AfterResetAsync,                                    //resume handler
                    "Are you sure you want to reset the count?",        //prompt text
                    "didn't get that!",                                 //retry string
                    3,                                                  //atempts
                    promptStyle: PromptStyle.None
                    );
            }
            else
            {
                await context.PostAsync($"{this.count++}: You said {message.Text}");
                context.Wait(MessageReceivedAsync); //Waits again, remembers dialog context until next message received.
            }
        }
        public async Task AfterResetAsync(IDialogContext context, IAwaitable<bool> argument)
        {
            var confirm = await argument;
            if (confirm)
            {
                this.count = 1;
                await context.PostAsync("Reset count.");
            }
            else
            {
                await context.PostAsync("Did not reset count.");
            }
            context.Wait(MessageReceivedAsync);
        }


    }
}