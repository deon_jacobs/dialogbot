﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace DialogBot.Models
{
    [LuisModel("6877c00c-6bee-4e56-83c4-d53a4d43e494", "b28fb990ca0e4a33939a6c7fc7373dca")]
    [Serializable]
    public class ConsultantRequestDialog : LuisDialog<object>
    {
        //Persist captured entities to enable dialog flow until all entities are received
        protected string roleValue = "Null";  
        protected string locationValue = "Null";
        
        [LuisIntent("ReqConsultant")]
        public async Task ConsultantRequest(IDialogContext context, LuisResult result)
        {
            EntityRecommendation role;
            EntityRecommendation location;
                        
            if (result.TryFindEntity("Role", out role)) { roleValue = role.Entity; }
            
            if (result.TryFindEntity("Location", out location)) { locationValue = location.Entity;}
            
            if (roleValue == "Null" || locationValue == "Null")
            {
                //Check which entities have not yet been captured and send message to retrieve the information
                if(roleValue =="Null")
                {
                    await context.PostAsync("Please specify the consultant role you are looking for.");
                    context.Wait(MessageReceived);
                }else if (locationValue == "Null")
                {
                    await context.PostAsync("I did not get the specific location were you require the consultant.");
                    context.Wait(MessageReceived);
                }
            }
            else
            {
                string confirmMessage = $"Can I run a query against the following Consultant features: Role - {roleValue}, Location - {locationValue}";
                PromptDialog.Confirm(
                   context,                                             //IDialog context
                   RunQueryAsync,                                       //resume handler
                   confirmMessage,
                   "I didn't get that!",                                //retry string
                   3,                                                   //atempts
                   promptStyle: PromptStyle.None
                   );
            }
            
        }
        public async Task RunQueryAsync(IDialogContext context, IAwaitable<bool> argument)
        {
            var confirm = await argument;
            if (confirm)
            {
                await context.PostAsync("Query Executed.");
            }
            else
            {
                await context.PostAsync("For some reason the query could not be exectued.");
            }
            resetEntities();
            context.Wait(MessageReceived);
        }
        private void resetEntities()
        {
            roleValue = "Null";
            locationValue = "Null";
        }
        [LuisIntent("Complaint")]
        public async Task ComplaintSubmit(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("You reqestered a complaint.");
            resetEntities();
            context.Wait(MessageReceived);
        }
        [LuisIntent("")]
        public async Task DidNotGetRequest2(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Sorry, I did not get your intent, please submit request from scratch.");
            resetEntities();
            context.Wait(MessageReceived);
        }

    }
}